
# Example scripts by Open Research Labs 

This folder provides a collection of reproducible examples for running
various data analyses and visualizations.

### Heatmaps

[Density heatmap](plot_density_heatmap.md)